import { createRouter, createWebHistory } from 'vue-router'


import Home from '../components/pages/Home.vue'
import About from '../components/pages/About.vue'
import Locations from '../components/pages/Location.vue'
import Departments from '../components/pages/Department.vue'
import Employees from '../components/pages/Employee.vue'
import NotFound from '../components/NotFound.vue'

const routes=[
    {
        path: '/',
        component: Home
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/location',
        component: Locations
    },
    {
        path: '/department',
        component: Departments
    },
    {
        path: '/employee',
        component: Employees
    },
    {
        path: '/:pathMatch(.*)*',
        component: NotFound
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router