# qarvue

This is the frontend part of the case study

##Description
We start using VUEJS using VITE and VUEUTIFY lib. Why this framework
1) Faster execution than REACTJS
2) Small learning curve
3) Possible successor of REACTJS (nowadays REACTJS is better for big projects)

##Configuration
1) git clone https://gitlab.com/iarv/qarvue.git
2) install visual studio code
3) open the project folder
4) install nodeJS
5) run: npm install
6) run: npm run dev
7) go to the URL provided on terminal (e.g. http://localhost:5173/)

##implementation
On the upper left corner of the screen there is a navigation label selection panel
Home | About | Locations | Departments | Employees

###Selection
Location:    Fetch all location info from backend
Departments: Fetch all departments from backend
Employees:   Fetch all employees from backend




